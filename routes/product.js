const express = require('express')
const router = express.Router()
const Product = require('../models/Product')

const getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getProduct = async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Product not found!!'
      })
    }
    res.json(product)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addProducts = async function (req, res, next) {
  // const newProduct = {
  //   id: lastId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // products.push(newProduct)
  // lastId++
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    return res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }

  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })
  // if (index >= 0) {
  //   products.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     Code: 404,
  //     msg: 'No Product id ' + req.params.id
  //   })
  // }
}

router.get('/', getProducts) // GET Products
router.get('/:id', getProduct)// Get One Product
router.post('/', addProducts)// Add New Product
router.put('/:id', updateProduct)
router.delete('/:id', deleteProduct)
module.exports = router
